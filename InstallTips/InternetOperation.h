//
//  InternetOperation.h
//  InstallTips
//
//  Created by David Sweeney on 07/02/2012.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "XMLParser.h"

@class XMLParser;

@interface InternetOperation : NSOperation
{
    ViewController* view;
}

- (void) main;

- (void) download_all_files;

- (void) download:(NSString*) s1 toFolder:(NSString*) s2;

- (void) set_view:(ViewController*)val;

@end
