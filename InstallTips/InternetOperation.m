//
//  InternetOperation.m
//  InstallTips
//
//  Created by David Sweeney on 07/02/2012.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import "InternetOperation.h"
#import "XMLParser.h"

@class XMLParser;

@implementation InternetOperation

- (void) main
{
    [self download_all_files];
}

- (void) download_all_files
{
    NSString* url_str = @"http://www.rsconnect.com/rsinstalls/structure.xml";
    
    NSURL* url = [[NSURL alloc] initWithString:url_str];
    NSData* d = [NSData dataWithContentsOfURL:url];
    NSString* s =[[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
    
    NSLog(@"Contents of structure.xml file: %@", s);
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true);
    NSString* documents_folder = [paths objectAtIndex:0];
    
    // Search through string for <folder>
    NSRange r, r2, r3, r4, r5, r6;
    NSString* folder_name = @"";
    NSMutableString* folder_path = [@"" mutableCopy];
    NSString* file_name = @"";
    NSMutableString* file_path = [@"" mutableCopy];
    NSFileManager* fm = [NSFileManager defaultManager];
    int end_folder_tag = 0;
    
    if ((s == nil) || ([s length] < 8)) {
        return;
    }
    
    NSLog(@"Length of s = %lu", (unsigned long)[s length]);
    
    for (int i = 0; i < ([s length] - 8); i++)
    {
        //NSLog(@"i = %d", i);
        
        r.location = i;
        r.length = 7;
        
        if ([[s substringWithRange:r] isEqualToString:@"<folder"]) {
            int j = (i + 14);
            int k = 0;
            while ([s characterAtIndex:j] != '\"') {
                j++;
                k++;
            }
            r2.location = (i + 14);
            r2.length = k;
            folder_name = [s substringWithRange:r2];
            
            // Check if a folder with this name exists
            folder_path = [documents_folder mutableCopy];
            [folder_path appendString:@"/"];
            [folder_path appendString:folder_name];
            BOOL is_dir = false;
            if ( (([fm fileExistsAtPath:folder_path isDirectory:&is_dir]) && is_dir) == false ) {
                // create folder
                NSLog(@"About to create folder: %@", folder_name);
                [fm createDirectoryAtPath:folder_path withIntermediateDirectories:true
                               attributes:nil error:nil];
                NSLog(@"Folder has now been created.");
            }
            
            // Search for </folder>
            end_folder_tag = (int)([s length] - 9);
            for (int n = i + 14; n < ([s length] - 9); n++) {
                r6.location = n;
                r6.length = 9;
                if ([[s substringWithRange:r6] isEqualToString:@"</folder>"]) {
                    end_folder_tag = n;
                    n = (int)([s length] - 8);
                }
            }
            
            // Search for <file>
            for (int l = i; l < end_folder_tag; l++) {
                r3.location = l;
                r3.length = 6;
                if ([[s substringWithRange:r3] isEqualToString:@"<file>"]) {
                    // Search for </file>
                    for (int m = l + 6; m < ([s length] - 14); m++) {
                        r4.location = m;
                        r4.length = 7;
                        
                        if ([[s substringWithRange:r4] isEqualToString:@"</file>"]) {
                            r5.location = l + 6;
                            r5.length = m - (l + 6);
                            file_name = [s substringWithRange:r5];
                            // Check if file exists...
                            file_path = [folder_path mutableCopy];
                            [file_path appendString:@"/"];
                            [file_path appendString:file_name];
                            
                            if ([fm fileExistsAtPath:file_path] == false) {
                                [self download:file_name toFolder:folder_name];
                            }
                            
                            m = (int)([s length] - 13);
                        }
                    }
                }
            }
            
            // After end of search for <file>
        }
    }
    
    NSLog(@"Outside the loop");
    
    // After they have all been downloaded...
    // Use dispatch async
    dispatch_async(dispatch_get_main_queue(), ^{
        [view download_finished];
    });
}

- (void) download:(NSString*) s1 toFolder:(NSString*) s2
{
    NSLog(@"About to try and download %@", s1);
    NSMutableString* url_str = [@"http://www.rsconnect.com/rsinstalls/" mutableCopy];
    
    NSMutableString* file_name = [s1 mutableCopy];
    [file_name replaceOccurrencesOfString:@" " withString:@"%20" options:NSLiteralSearch
                                    range:NSMakeRange(0, [file_name length])];
    
     
    // replace e umlaut with %C3, so that it forms a valid URL
    [file_name replaceOccurrencesOfString:@"ë" withString:@"%C3%AB" options:NSLiteralSearch
                                    range:NSMakeRange(0, [file_name length])];
    
    [url_str appendString:file_name];
    NSURL* url = [[NSURL alloc] initWithString:url_str];
    
    if ((s1 == nil) || ([s1 isEqualToString:@""])) {
        return;
    }
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                          NSUserDomainMask, true);
    NSString* fp = [paths objectAtIndex:0];
    fp = [fp stringByAppendingFormat:@"/%@/%@", s2, s1];
    
    NSData* d = [NSData dataWithContentsOfURL:url];
    [d writeToFile:fp atomically:true];
    NSLog(@"I have just saved a file to this path: %@", fp);    
}

- (void) set_view:(ViewController*) val
{
    view = val;
}

@end
