//
//  AppDelegate.m
//  InstallTips
//
//  Created by David Sweeney on 11/01/2012.
//  Joel Holland thought of the original idea.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.

    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    folder_path = [[paths objectAtIndex:0] mutableCopy];
    
    NSLog(@"Documents: ");
    NSLog(@"%@", folder_path);
    
    // If folders do not exist, create them
    // Create about 37 subfolders
    car_folder[0] = @"Alfa Romeo";
    car_folder[1] = @"Audi";
    car_folder[2] = @"Bmw";
    car_folder[3] = @"Chevrolet";
    car_folder[4] = @"Chrysler";
    car_folder[5] = @"Citroen";
    car_folder[6] = @"Daihatsu";
    car_folder[7] = @"Fiat";
    car_folder[8] = @"Ford";
    car_folder[9] = @"Honda";
    car_folder[10] = @"Hyundai";
    car_folder[11] = @"Jeep";
    car_folder[12] = @"Kia";
    car_folder[13] = @"Land rover";
    car_folder[14] = @"Lexus";
    car_folder[15] = @"Lotus";
    car_folder[16] = @"Mazda";
    car_folder[17] = @"Mercedes-Benz";
    car_folder[18] = @"MG";
    car_folder[19] = @"Microcar";
    car_folder[20] = @"Mini";
    car_folder[21] = @"Mitsubishi";
    car_folder[22] = @"Nissan";
    car_folder[23] = @"Peugeot";
    car_folder[24] = @"Renault";
    car_folder[25] = @"Rover";
    car_folder[26] = @"Saab";
    car_folder[27] = @"Seat";
    car_folder[28] = @"Skoda";
    car_folder[29] = @"Smart";
    car_folder[30] = @"Subaru";
    car_folder[31] = @"Suzuki";
    car_folder[32] = @"Toyota";
    car_folder[33] = @"Vauxhall";
    car_folder[34] = @"Volvo";
    car_folder[35] = @"VW";
    
    NSMutableString* subfolder_path = [@"" mutableCopy];
    NSFileManager* fm = [NSFileManager defaultManager];
    for (int i = 0; i < FOLDERS; i++) {
        subfolder_path = [folder_path mutableCopy];
        if (car_folder[i] != nil) {
            if (! [car_folder[i] isEqualToString:@""]) {
                [subfolder_path appendFormat:@"/%@", car_folder[i]];
                if (![fm fileExistsAtPath:subfolder_path isDirectory:nil]) {
                    BOOL success = [fm createDirectoryAtPath:subfolder_path withIntermediateDirectories:YES attributes:nil error:nil];
                    if (! success) {
                        NSLog(@"Error: create folder failed %@", subfolder_path);
                    }
                }
            }
        }
    }

    // Set up the various windows
    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
