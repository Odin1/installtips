//
//  ViewController.h
//  InstallTips
//
//  Created by David Sweeney on 11/01/2012.
//  Joel Holland thought of the original idea.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    // GUI
    IBOutlet UIButton* move_up_button;
    IBOutlet UIButton* show_browser_button;
    IBOutlet UIButton* download_all;
    IBOutlet UITableView* table;
    
    UIActivityIndicatorView* wheel;
    
    IBOutlet UILabel* message_label;
        
    // Instance vars
    NSMutableArray* files;
    int file_index;
    NSString* file_path_array[6];
    NSString* docs_folder;
    
    BOOL download_active;    // If true, then download is actually taking place.
    int num_times_download_pressed;
}

// Callback functions
- (IBAction) move_up_button_pressed:(id)sender;

- (IBAction) show_browser:(id)sender;

- (IBAction) download_all_pressed:(id)sender;

- (IBAction) refresh_pressed:(id)sender;

// Other methods
- (void) download_all_files;

- (NSMutableArray*) list_files;

- (void) move_down:(NSString*) fp;

- (void) move_up;

- (void) refresh;

- (void) download_finished;

- (void) start_wheel;

- (void) stop_wheel;

@end
