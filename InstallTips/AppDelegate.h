//
//  AppDelegate.h
//  InstallTips
//
//  Created by David Sweeney on 11/01/2012.
//  Joel Holland thought of the original idea.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FOLDERS 36

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSMutableString* folder_path;
    NSString* car_folder[FOLDERS];
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;




@end
