//
//  FileViewController.m
//  InstallTips
//
//  Created by David Sweeney on 11/01/2012.
//  Joel Holland thought of the original idea.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import "FileViewController.h"
#import "ViewController.h"

@class ViewController;

@implementation FileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // I do not need to add any code to this method.
}

#pragma mark - View lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Image of PDF";
    
    // Make sure you can use web_view to zoom in and zoom out
    web_page_view.scalesPageToFit = true;
    
    [self show_pdf_at_file_path];
}

- (void) viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation == UIInterfaceOrientationPortrait) {
        return true;
    } else {
        return false;
    }
}

- (IBAction) delete_file_pressed:(id)sender
{
    // Are you sure?
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Delete" message:@"Are you sure?"
        delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (IBAction) back_pressed:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

// AlertView callback function: Response to "Are you sure?"
- (void) alertView:(UIAlertView*) alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // If "Yes" pressed
    if (buttonIndex == 1)
    {
        if ([alertView.title isEqualToString:@"Delete"])
        {
            // Delete the current file
            NSFileManager* fm = [NSFileManager defaultManager];
            if ([fm fileExistsAtPath:file_path]) {
                [fm removeItemAtPath:file_path error:nil];
                [vc refresh];
                [self dismissViewControllerAnimated:true completion:nil];
            }            
        }
    }
}

- (void) show_pdf_at_file_path
{
    @try {
        NSString* s = [NSString stringWithFormat:@"file://%@", file_path];
        s = [s stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        s = [s stringByReplacingOccurrencesOfString:@"ë" withString:@"%C3%AB"];
        
        /*
         [s replaceOccurrencesOfString:@" " withString:@"%20" options:NSLiteralSearch
         range:NSMakeRange(0, [s length])];
         
         // replace e umlaut with %C3, so that it can be opened in UIWebView
         [s replaceOccurrencesOfString:@"ë" withString:@"%C3%AB" options:NSLiteralSearch
         range:NSMakeRange(0, [s length])];
         */
        
        NSURL* url = [[NSURL alloc] initWithString:s];
        NSURLRequest* url_req = [[NSURLRequest alloc] initWithURL:url];
        [web_page_view loadRequest:url_req];
    } @catch (NSException *exception) {
        NSLog(@"show_pdf_at_file_path. Exception: %@", exception.description);
    }
}

- (void) set_folder_path:(NSString*) val
{
    folder_path = val;
}

- (void) set_file_path:(NSString*) val
{
    file_path = val;
}

- (void) set_view_controller:(ViewController*) val
{
    vc = val;
}

- (void) set_url:(NSString*) val
{
    NSURL* url = [[NSURL alloc] initWithString:val];
    NSURLRequest* url_req = [[NSURLRequest alloc] initWithURL:url];
    [web_page_view loadRequest:url_req];
}

- (void) disable_delete
{
    delete_button.enabled = false;
}

- (void) enable_delete
{
    delete_button.enabled = true;
}

@end
