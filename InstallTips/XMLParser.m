//
//  XMLParser.m
//  InstallTips
//
//  Created by David Sweeney on 20/12/2012.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import "XMLParser.h"

@implementation XMLParser

- (void) set_text:(NSString*) val
{
    text = [val copy];
}

- (void) set_path:(NSString*) val
{
    path = [val copy];
}

- (NSString*) get_data_at_path:(NSString*) p
{
    NSLog(@"Inside get_data_at_path");

    path = [p copy];
    char c = (char) [path characterAtIndex:([path length] - 1)];
    if (c != '/') {
        path = [NSString stringWithFormat:@"%@/", p];
    }
    
    NSLog(@"path = %@", path);
    
    if (text == nil) {
        text = @"";
        return @"";
    }
    
    // Count number of forward slashes in path
    int count = 0;
    NSMutableArray* slash_array = [[NSMutableArray alloc] init];
    for (int r = 0; r < [path length]; r++) {
        char c = (char) [path characterAtIndex:r];
        if (c == '/' || (r == ([path length] - 1))) {
            NSNumber* num = [[NSNumber alloc] initWithInt:r];
            [slash_array addObject:num];
            count++;
        }
    }
    
    if (count == 0) {
        return @"";
    }
    
    NSString* subtext = [text copy];
    NSString* tag = @"";
    NSString* start_tag = @"";
    NSString* end_tag = @"";
    
    for (int i = 0; i < (count - 1); i++) {
        NSNumber* start_num = (NSNumber*) [slash_array objectAtIndex:i];
        int start = [start_num intValue];
        NSNumber* end_num = (NSNumber*) [slash_array objectAtIndex:(i + 1)];
        int end = [end_num intValue];
        
        NSRange r;
        r.location = (start + 1);
        r.length = (end - start - 1);
        
        tag = [path substringWithRange:r];
        //NSLog(@"tag = %@", tag);
        // See if tag contains square brackets [ ] at the end
        int tag_index = 1;
        
        // Look at the part of tag with [1] or [3] or [33] or [444]
        NSString* suffix = @"";
        int last_char = (int)([tag length] - 1);
        char c = (char) [tag characterAtIndex:last_char];
        if (c == ']') {
            //NSLog(@"It has [x] at the end.");
            // Loop through tag, looking for [
            int open_bracket = 0;
            BOOL open_found = FALSE;
            for (int j = 0; j < last_char; j++) {
                c = (char) [tag characterAtIndex:j];
                if (c == '[') {
                    open_bracket = j;
                    open_found = TRUE;
                    //NSLog(@"open_bracket = %d", j);
                }
            }
            
            if (open_found == TRUE) {
                NSRange r7;
                r7.location = open_bracket + 1;
                r7.length = last_char - open_bracket - 1;
                suffix = [tag substringWithRange:r7];
                //NSLog(@"suffix = %@", suffix);
                tag_index = [suffix intValue];
            }
        } else {
            //NSLog(@"It does not have [ ] at the end.");
            tag_index = 1;
        }
        
        //NSLog(@"tag_index = %d", tag_index);

        // Remove the square brackets from the start_tag
        NSString* short_tag = @"";
        if ([suffix length] > 0) {
            int zz = (int)([tag length] - [suffix length] - 2);
            short_tag = [tag substringToIndex:zz];
        } else {
            short_tag = [tag copy];
        }
        
        start_tag = [NSString stringWithFormat:@"<%@", short_tag];
        //NSLog(@"start_tag = %@", start_tag);
        end_tag = [NSString stringWithFormat:@"</%@>", short_tag];
        //NSLog(@"end_tag = %@", end_tag);
        
        // Find instance number tag_index of <start_tag>
        NSRange r2;
        
        int count_b = 0;
        for (int j = 0; j < ([subtext length] - [start_tag length]); j++) {
            NSString* s3 = @"";
            NSRange r6;
            r6.location = j;
            r6.length = [start_tag length];
            s3 = [subtext substringWithRange:r6];

            //
            if ([s3 isEqualToString:start_tag]) {
                //NSLog(@"s3 equals start_tag");
                count_b++;
                if (count_b == tag_index) {
                    //NSLog(@"count_b equals tag_index");
                    r2.location = j;
                    r2.length = [start_tag length];
                    break;
                }
            }
        }
        
        //NSLog(@"count_b = %d", count_b);
        
        int x = (int)r2.location;
        // Find the first > after the start_tag
        for (int k = (int)(r2.location + 1); k < [subtext length]; k++) {
            char c = (char) [subtext characterAtIndex:k];
            if (c == '>') {
                x = k;
                break;
            }
        }
        int len = (int)(x - r2.location + 1);
        
        // Find where end tag is
        NSRange r3 = [subtext rangeOfString:end_tag];
        
        if ((r2.location == NSNotFound) || (r3.location == NSNotFound)) {
            return @"<Item not found>";
        }
        
        int start_2 = (int)(r2.location + len);
        int end_2 = (int)r3.location;
        
        NSRange r4;
        r4.location = start_2;
        r4.length = (end_2 - start_2);
        NSString* subtext_2 = [subtext substringWithRange:r4];
        //NSLog(@"subtext_2 = %@", subtext_2);
        
        subtext = [subtext_2 copy];
    }
    
    return subtext;
}


@end
