//
//  FileViewController.h
//  InstallTips
//
//  Created by David Sweeney on 11/01/2012.
//  Joel Holland thought of the original idea.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface FileViewController : UIViewController
{
    IBOutlet UIWebView* web_page_view;
    IBOutlet UIButton* delete_button;
    
    NSString* folder_path;
    NSString* file_path;
    
    // Reference to ViewController object
    ViewController* vc;
}

- (IBAction) delete_file_pressed:(id)sender;

- (IBAction) back_pressed:(id)sender;



- (void) set_folder_path:(NSString*)val;

- (void) set_file_path:(NSString*) val;

- (void) set_view_controller:(ViewController*)val;

- (void) set_url:(NSString*)val;


// Dealing with the "Delete" button
- (void) disable_delete;

- (void) enable_delete;


@end
