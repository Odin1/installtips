//
//  ViewController.m
//  InstallTips
//
//  Created by David Sweeney on 11/01/2012.
//  Joel Holland thought of the original idea.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import "ViewController.h"
#import "FileViewController.h"
#import "InternetOperation.h"

@implementation ViewController

@class InternetOperation;

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self refresh];
    
    download_active = false;
    num_times_download_pressed = 0;
    
    files = [[NSMutableArray alloc] init];

    table.delegate = self;
    table.dataSource = self;
        
    for (int i = 0; i < 6; i++) {
        file_path_array[i] = @"";
    }
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSMutableString* folder_path = [[paths objectAtIndex:0] mutableCopy];
    
    file_path_array[0] = [folder_path copy];
    docs_folder = [folder_path copy];
    
    [self list_files];
    
    wheel = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    wheel.center = CGPointMake(690, 70);
	wheel.hidesWhenStopped = true;
    [self.view addSubview:wheel];
}

- (void) viewDidUnload
{
    /*
    table = nil;
    move_up_button = nil;
    show_browser_button = nil;
    download_all = nil;
    message_label = nil;
    */
    
    [super viewDidUnload];
}

// Methods to do with the view appearing and disappearing
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation == UIInterfaceOrientationPortrait) {
        return true;
    } else {
        return false;
    }
}

// Delegate methods relating to the table
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int k = (int)indexPath.row;
    
    if ((k < 0) || (k > [files count])) {
        return;
    }
    
    NSString* file_name = [files objectAtIndex:k];
    NSMutableString* current_path = [file_path_array[file_index] mutableCopy];
    [current_path appendString:@"/"];
    [current_path appendString:file_name];

    NSLog(@"current_path = %@", current_path);
    
    if (([file_name hasSuffix:@".pdf"]) || ([file_name hasSuffix:@".jpg"]) ||
        ([file_name hasSuffix:@".jpeg"]))
    {
        FileViewController* fvc = [[FileViewController alloc]
                                   initWithNibName:@"FileViewController" bundle:nil];
        [fvc set_view_controller:self];
        [fvc enable_delete];
        [fvc set_file_path:current_path];
        [self presentViewController:fvc animated:true completion:nil];
    }
    else
    {
        // Move down into subfolder
        [self move_down:current_path];
        [self list_files];
    }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [files count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    int n = (int)indexPath.row;
    if ((n < 0) || (n > [files count])) {
        return cell;
    }
    NSString* f = [files objectAtIndex:n];
    
    // set the text of the file's name
    cell.textLabel.text = f;
    
    // Configure the cell.
    return cell;
}

// Callback methods from the user interface
- (IBAction) move_up_button_pressed:(id)sender
{
    [self move_up];
}

- (IBAction) show_browser:(id)sender
{
    // Show Browser in a separate window
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
            @"http://www.rsconnect.com/rsinstalls"]];
}

- (IBAction) download_all_pressed:(id)sender
{
    NSLog(@"Inside download all pressed");
    
    num_times_download_pressed++;
    
    if (num_times_download_pressed >= 2) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@""
            message:@"Download is already in progress."
            delegate:self
            cancelButtonTitle:@"OK"
            otherButtonTitles:nil];
        [alert show];
        return;
    } else {
        download_active = true;
        [self download_all_files];
    }
}

// It would be nice to use async_dispatch here

- (void) download_all_files
{
    download_active = true;
    
    [self start_wheel];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        InternetOperation* op = [[InternetOperation alloc] init];
        [op set_view:self];
        [op download_all_files];
    });
    
    //NSOperationQueue* q = [[NSOperationQueue alloc] init];
    //[q addOperation:op];
}

- (IBAction) refresh_pressed:(id)sender
{
    [self refresh];
}

// Creates a list of files
- (NSMutableArray*) list_files
{
    files = [[NSMutableArray alloc] init];
    
    NSFileManager* fm = [NSFileManager defaultManager];
    NSArray* paths = [fm contentsOfDirectoryAtPath:file_path_array[file_index] error:nil];
    
    for (int i = 0; i < [paths count]; i++) {
        NSString* s = [paths objectAtIndex:i];
        if ((! [s hasPrefix:@"."]) && (! [s isEqualToString:@"log.txt"])) {
            [files addObject:s];
        }
    }
    
    [table reloadData];
    
    return files;
}

// Move down one level through the folder tree
- (void) move_down:(NSString*) fp
{
    if (file_index <= 4) {
        file_index++;
        file_path_array[file_index] = fp;
        
        // may need to refresh list of files
        [self refresh];
    }
}

// Move up one level through the folder tree
- (void) move_up
{
    if (file_index == 0) {
        return;
    }
    
    if (file_index >= 1) {
        for (int i = file_index; i <= 5; i++) {
            file_path_array[i] = @"";
        }
        file_index--;
        
        // may need to refresh list of files
        [self list_files];
    }
    
    [self refresh];
}

- (void) refresh
{
    if (file_index == 0) {
        move_up_button.hidden = true;
    } else {
        move_up_button.hidden = false;
    }
 
    message_label.text = @"Message: ";
    [self list_files];
}

- (void) download_finished
{
    NSLog(@"Inside download_finished");
    
    download_active = false;
    num_times_download_pressed = 0;
    
    [self stop_wheel];
    [self refresh];
    
    // Display a label saying "Download complete"
    message_label.text = @"Message: Download complete.";
}

- (void) start_wheel
{
    [wheel startAnimating];
}

- (void) stop_wheel
{
    [wheel stopAnimating];
}

@end
