//
//  XMLParser.h
//  InstallTips
//
//  Created by David Sweeney on 20/12/2012.
//  Copyright (c) 2012 RS Fleet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLParser : NSObject
{
    NSString* text;
    
    NSString* path;
}

- (void) set_text:(NSString*) val;

- (void) set_path:(NSString*) val;

- (NSString*) get_data_at_path:(NSString*) p;

@end
